#!/bin/zsh


## system info ##
chassis_name() {
    # 1 Other
    # 2 Unknown
    # 3 Desktop
    # 4 Low Profile Desktop
    # 5 Pizza Box
    # 6 Mini Tower
    # 7 Tower
    # 8 Portable
    # 9 Laptop
    # 10 Notebook
    # 11 Hand Held
    # 12 Docking Station
    # 13 All in One
    # 14 Sub Notebook
    # 15 Space-Saving
    # 16 Lunch Box
    # 17 Main System Chassis
    # 18 Expansion Chassis
    # 19 SubChassis
    # 20 Bus Expansion Chassis
    # 21 Peripheral Chassis
    # 22 Storage Chassis
    # 23 Rack Mount Chassis
    # 24 Sealed-Case PC
    chassis_code=`cat /sys/class/dmi/id/chassis_type`
    if [[ "$chassis_code" == "3" ]]; then
        echo desktop
    elif [[ "$chassis_code" == "9" ]]; then
        echo laptop
    elif [[ "$chassis_code" == "10" ]]; then
        echo laptop
    else
        echo unknown
    fi
}

populate_ssd_info(){
    local blocks=`ls /sys/block/`
    ssd_info=""
    for file in /sys/block/*; do
        filename=$(basename $file)
        rotational=$(cat ${file}/queue/rotational)
        # if it's rotational it's not an ssd
        [[ $rotational = 1 ]] && value="hdd" || value="ssd"
        # creating variable disk_ssd
        eval "${filename}=$value"
        ssd_info="$ssd_info ${filename}=$value"
    done
    ssd_info=$(echo $ssd_info | sed 's/ //') # stripping first space
}

populate_system_info() {
    populate_ssd_info
    ## this are the exported variables
    hostname=`hostname`
    chassis=$(chassis_name)
    distro=`lsb_release -si`
    ### ! system info ###
}


### Colors ###
export COLOR_NC='\e[0m' # No Color
export COLOR_WHITE='\e[1;37m'
export COLOR_BLACK='\e[0;30m'
export COLOR_BLUE='\e[0;34m'
export COLOR_LIGHT_BLUE='\e[1;34m'
export COLOR_GREEN='\e[0;32m'
export COLOR_LIGHT_GREEN='\e[1;32m'
export COLOR_CYAN='\e[0;36m'
export COLOR_LIGHT_CYAN='\e[1;36m'
export COLOR_RED='\e[0;31m'
export COLOR_LIGHT_RED='\e[1;31m'
export COLOR_MAGENTA='\e[0;35m'
export COLOR_LIGHT_MAGENTA='\e[1;35m'
export COLOR_BROWN='\e[0;33m'
export COLOR_YELLOW='\e[1;33m'
export COLOR_GRAY='\e[0;30m'
export COLOR_LIGHT_GRAY='\e[0;37m'
### ! Colors ###

CFC_COMMAND=$0

help(){
    echo "cfc -- Config File Compiler"
    echo "usage: $CFC_COMMAND -i <input file> -o <output file>"
    echo "optional arguments"
    echo "-c [comment char] -- comment char to be used by the script, default is ####"
    echo "-k -- keep going after a fail, default is false"
    echo "-d -- print debug"
    echo "-a -- append mode, does not delete output file before writing into it"
    echo "-s -- silent, no output at all"
    echo "-e -- error only, only outputs error messages (without the ERROR:)"

    if [[ $1 ]]; then
        echo 
        echo The following variables are avaliable to substitution
        echo \$hostname = the result of executing hostname
        echo \$chassis = chassis name, desktop, laptop or unknown, converted from /sys/class/dmi/id/chassis_type
        echo \$distro = result of lsb_release -si
        echo "\$<disk> = whether <disk> is an ssd or hdd, for example sda=hdd, you can print $ssd_info to see all disks"
        
    fi
    exit 1
}

while getopts i:o:hc:kdase OPTION; do
    case "${OPTION}" in
        i) input_file=${OPTARG} ;;
        o) OUTPUT_FILE=${OPTARG} ;;
        h) print_help=1 ;;
        c) comment_char=${OPTARG} ;;
        k) KEEP_GOING=1 ;;
        d) DEBUG=1 ;;
        a) APPEND=1 ;;
        s) SILENT=1 ;;
        e) ERROR_ONLY=1 ;;
    esac
done

[ $print_help ] && help 1
[ $input_file ] || help 
[ $OUTPUT_FILE ] || help 
[ $comment_char ] || comment_char="####"
# if silent, ignore error only and debug
if [[ $SILENT ]]; then
    unset ERROR_ONLY
    unset DEBUG
fi
# if error only ignore debug
[ $ERROR_ONLY ] && unset DEBUG

if [[ $KEEP_GOING ]]; then
    COLOR_ERROR=$COLOR_YELLOW
    COLOR_WARNING=$COLOR_BLUE
else
    COLOR_ERROR=$COLOR_RED
    COLOR_WARNING=$COLOR_YELLOW
fi

populate_system_info

input_folder=$(dirname ${input_file})

if [[ $DEBUG ]]; then
    echo
    echo "### Params ###"
    echo input file $input_file
    echo output file $OUTPUT_FILE
    echo comment char $comment_char
    [ $APPEND ] && echo Append mode
    [ $KEEP_GOING ] && echo Keep Going
    echo "### ! Params ###"
    echo

    # source system_info #imports a bunch of system information, on a sepparate file to keep things clean
    echo "### System info ###"
    echo Host: $hostname
    echo Chassis: $chassis
    echo Distro $distro
    echo SSD info $ssd_info
    echo "### ! System info ###"
    echo
fi

if [[ -f $input_file ]]; then
    if [[ ! $APPEND ]]; then
        # [[ -f $OUTPUT_FILE ]] && rm $OUTPUT_FILE
        echo "$comment_char AUTOGENERATED FILE, DO NOT EDIT." > $OUTPUT_FILE
        echo "$comment_char GENERATED USING $0 ${@}" >> $OUTPUT_FILE
        echo "$comment_char IN FOLDER `pwd`" >> $OUTPUT_FILE
    fi



    # A condition is difined as check:condition but it can have a whitespace or not
    remove_first_condition() {
        line=$1
        actual_line="${line#*:}"
        if [[ "$actual_line[1,1]" == " " ]]; then
            actual_line="${actual_line# *}"
        fi
        echo $actual_line

    }

    build_sed_command() {
        command="sed "
        for p in $@
        do
            param=("${(@s/=/)p}") #split to [1]=[2]
            pattern=$param[1]
            replace=$param[2]
            # Uing | as sed delimeters because / will most likely be used in path subrtitution
            # We want to find pattern when preceded by $ to avoid unwanted substitutions
            # it can be $pattern or ${pattern} to allow ${pattern}_foo
            # notice that $pattern_foo will not be replaced
            command="$command -e 's|\$$pattern\\b|$replace|g' -e 's|\${$pattern}|$replace|g'"
        done
        echo $command
    }

    cfc_file() {
        if [[ $DEBUG ]];then
            debug_param="-d"
        else
            # if not on debug get only errors
            debug_param="-e"
        fi
        [ $KEEP_GOING ] && keep_going="-k"
        cfc_command="$CFC_COMMAND $debug_param $keep_going -c \"$comment_char\" -a -o $OUTPUT_FILE  -i $params"
        if [[ $DEBUG ]]; then
            echo ${COLOR_CYAN}running cfc $cfc_command${COLOR_NC}
        fi
        cfc_output=$(eval $cfc_command)
        if [[ $? != 0 ]]; then
            ERROR="problem parsing cfc file, log below:

### Log for file $params ###

$cfc_output

${COLOR_ERROR}### Log end $params ###${COLOR_NC}" # color might have been disabled in log
            return 1
        fi
        return 0
    }
    
    import_file() {
        input_file=$input_folder/$1
        if [[ ! -f $input_file ]]; then
            ERROR="File not found $input_file"
            return 1
        fi
        
        shift # removing $1 from arguments
        command="cat $input_file"
        if [[ $#@ -ge 1 ]]; then # if there are some params still
            [ $DEBUG ] && echo using substitutions $@
            params=($@)
            command=$(build_sed_command $params)
            command="$command $input_file"
        fi
        command="$command >> $OUTPUT_FILE"
        [ $DEBUG ] && echo $command
        eval $command
        return 0
    }
    get_file_with_args_from_line () {
        removed_import=${1#import}
        stripped_file_name=$(echo $removed_import | sed 's/ //')
        echo $stripped_file_name
    }
    process_import_line() {
        local file_name=$1
        shift # removing $1 from arguments
        # params=($as_array[2,$#as_array]) # Create a new array (()) formed of as_array[2] until as_array size ($#as_array)
        local params=($@)
        import_file $file_name $params
        could_import=$?
        if [[ $could_import -eq 0 ]];then
            [ ! $SILENT ] && echo ${COLOR_GREEN}Imported $file_name${COLOR_NC}
        fi
        return $could_import
    }

    ## Populates CONDITION and COMMAND from a line as CONDITION:COMMAND
    split_colon(){
        line=${(j: :)@} #concat params with a space in between
        CONDITION=${line%:*} # Stripped of :
        # user might have typed condition:command instead of condition: command
        local after_colon=${line#*:} # checking if there's stuff after :
        if [[ $after_colon == $CONDITION ]]; then # if there is stuff just remove the
            COMMAND=""
        else
            COMMAND=("${(@s/ /)after_colon}") # resplit the command, since it became a string
            
        fi

    }
    
    process_if_condition() {
        # $1 expected value
        local expected_value=$1
        shift # removing $1 from arguments
        split_colon $@
        param_value=$CONDITION
        params=($COMMAND)

        if [[ $param_value == $expected_value ]]; then
            process_line $params
            return $?
        else
            if [[ $DEBUG ]]; then
                echo ${COLOR_CYAN}Ignoring command $params${COLOR_NC}
            fi
        fi
        return 0
    }

    try_line() {
        TRYING=1
        process_line $params
        # if an error happen, report it but don't send it forward
        if [[ $? != 0 ]]; then
            echo ${COLOR_WARNING}Try failed: $params
            echo Reason: $ERROR${COLOR_NC}
        fi
        unset TRYING
        return 0
    }
    ssd_if() {
        # We can be expecting it to be a ssd or a hdd
        expected=$1
        shift #removind expected value from params
        params=($@)
        split_colon $params
        disk_name=$CONDITION
        # Geting the value of disk_name, either hdd or sdd, this are stored in $sda, $sdb etc
        actual_disk_type=`eval echo "$"${disk_name}`
        if [[ ! $actual_disk_type ]]; then
            ERROR="Disk not found $disk_name"
            return 1
        fi


        # adding a : because process_if_condition expects to split at ':'
        # <expected disk type> <actual disk type>: $COMMAND
        process_if_condition $expected ${actual_disk_type}: $COMMAND
    }

    begin_parse_script() {
        PARSING_SCRIPT=1
        split_colon $@ # user can name an script by passing a params:name
        SCRIPT_NAME=($COMMAND)
        SCRIPT_COMMAND=${(j: :)CONDITION} #concat params with a space in between
        # echo $SCRIPT_COMMAND
        if [[ ! $SCRIPT_COMMAND ]]; then
            SCRIPT_COMMAND="."
        fi
        SCRIPT=/tmp/$(uuidgen)
        touch $SCRIPT
        if [[ $DEBUG ]]; then
            echo ${COLOR_CYAN}writing script $SCRIPT${COLOR_NC}
        fi    
    }

    begin_parse_text() {
        PARSING_TEXT=1
        split_colon $@ # user can name an script by passing a params:name
        TEXT_NAME=($COMMAND)
        #TEXT_COMMAND="$(build_sed_command $params) -"
        TEXT_SUBSTITUTIONS=($CONDITION)
        if [[ $DEBUG ]]; then
            echo ${COLOR_CYAN}Starting text parse${COLOR_NC}
            if [[ $#TEXT_SUBSTITUTIONS -ge 1 ]]; then
                echo ${COLOR_CYAN}with substitutions${COLOR_NC}
                for p in $TEXT_SUBSTITUTIONS
                do
                    param=("${(@s/=/)p}") #split to [1]=[2]
                    echo $param[1] for $param[2]
                done
            fi
        fi        
    }



    process_line(){
        local command=$1
        shift # removing $1 from arguments
        local params=($@)
        if [[ $command == "import" ]]; then
            process_import_line $params
        elif [[ $command == "cfc" ]]; then
            cfc_file $params
        elif [[ $command == "try" ]]; then
            try_line $params
        elif [[ $command == "if" ]]; then
            process_if_condition $params
        elif [[ $command == "hostname" ]]; then
            process_if_condition $hostname $params
        elif [[ $command == "distro" ]]; then
            process_if_condition $distro $params
        elif [[ $command == "chassis" ]]; then
            process_if_condition $chassis $params
        elif [[ $command == "ssd" || $command == "hdd" ]]; then
            ssd_if $command $params
        elif [[ $command == "script" ]]; then
            begin_parse_script $params
        elif [[ $command == "text" ]]; then
            begin_parse_text $params
        else
            ERROR="Unable to understand command: $command"
            return 1
        fi

        return $?
    }


    [ ! $SILENT ] && [ ! $ERROR_ONLY ] && echo "###### Start processing $input_file ######"
    line_number=1
    IFS=''
    cat $input_file |
        while read line
        do

            ## If we're parsing a script do the parse in the corresponding manner
            if [[ $PARSING_SCRIPT -eq 1 ]]; then
                if [[ $line[1,7] == "!script" ]]; then
                    if [[ $DEBUG ]]; then
                        echo "done with current script"
                        echo ${COLOR_CYAN}Executing script with $SCRIPT_COMMAND${COLOR_NC}
                        cat $SCRIPT
                    fi
                    eval "$SCRIPT_COMMAND $SCRIPT >> $OUTPUT_FILE"
                    [ ! $SILENT ] && echo ${COLOR_GREEN}Imported script $SCRIPT_NAME${COLOR_NC}
                    rm $SCRIPT
                    PARSING_SCRIPT=0
                else
                    echo $line >> $SCRIPT
                    # If I have a command keep things in a file and run it at the end

                    #if [[ $SCRIPT_COMMAND ]]; then
                    #    echo $line >> $SCRIPT
                    #else # otherwise eval line by line
                    #    eval $line
                    #fi
                fi
                continue
            elif [[ $PARSING_TEXT -eq 1 ]]; then
                if [[ $line[1,5] == "!text" ]]; then
                    PARSING_TEXT=0
                    if [[ $DEBUG ]]; then
                        echo ${COLOR_CYAN}Done text parse${COLOR_NC}
                    fi
                    [ ! $SILENT ] && echo ${COLOR_GREEN}Imported text $TEXT_NAME${COLOR_NC}
                else
                    # line=${line:s/\$/\\\$}
                    for p in $TEXT_SUBSTITUTIONS
                    do
                        param=("${(@s/=/)p}") #split to [1]=[2]
                        pattern="\$$param[1]"
                        replace=$param[2]
                        l="line=\${line:s|${pattern}|${replace}}"
                        eval $l
                    done
                    line=`eval echo \"$line\"` # evaluating the line to replace variables like $distro or $chassis
                    echo $line >> $OUTPUT_FILE
                    
                fi
                continue
            fi


            
            # Stripping comments
            line=${line%#*} # from $1 remove (%) everything (*) after the first #
            ## TODO perhaps if no ; was found keep appending lines until one is found
            line=${line%;*} # Remove everything after semicolon
            line=`eval echo $line` # evaluating the line to replace variables like $distro or $chassis
            if [[ ! -z $line ]]; then
                # Split line by the spaces into command and arguments
                args=("${(@s/ /)line}")
                command=$args[1]
                params=($args[2,$#args]) # Create a new array (()) formed of as_array[2] until as_array size
                process_line $command $params
                process_result=$?
                if [[ $process_result -ne 0 ]]; then
                    if [[ $ERROR_ONLY ]]; then
                        echo Error on line $line_number in file $input_file
                        echo $ERROR
                    elif [[ ! $SILENT ]]; then
                        echo ${COLOR_ERROR}
                        echo Error on line $line_number in file $input_file
                        echo $line
                        echo Error: $ERROR${COLOR_NC}
                    fi
                    

                    if [[ ! $KEEP_GOING ]]; then
                        echo
                        exit $process_result
                    else
                        echo ${COLOR_MAGENTA}keep going${COLOR_NC}
                        echo
                    fi
                       
                    
                fi
            fi
#        fi
        let "line_number++"
    done

    [ ! $SILENT ] && [ ! $ERROR_ONLY ] && echo "###### Done processing $input_file ######"
    
else
    echo ${COLOR_ERROR}"ERROR: input file $input_file does not exist"${COLOR_NC}
    return 1
fi
