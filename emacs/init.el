;; Basic config
(progn
  (tool-bar-mode -1)
  (menu-bar-mode -1)
  (scroll-bar-mode -1))

(setq inhibit-startup-message t)   ;; hide the startup message
(setq initial-scratch-message nil) ;; scratch comes with a default message


(global-unset-key (kbd "C-z"))     ;; Fu**ing Ctrl+z
(global-set-key (kbd "C-;") 'comment-or-uncomment-region)
(global-set-key (kbd "RET") 'newline-and-indent)
(setq require-final-newline t)
(fset 'yes-or-no-p 'y-or-n-p)
(ido-mode t)
(setq-default indent-tabs-mode nil)

(global-visual-line-mode t)

(electric-pair-mode 1)          ;; old autopair
(show-paren-mode t)             ;; Highlights matching () [] {}
(setq show-paren-style 'mixed)  ;; Highlight matching if possible
(setq org-startup-indented t)   ;; Use org-indent


(set-frame-font "Hack Nerd Font 12" nil t)

;; First time config to get use-package working
(require 'package)
(toggle-debug-on-error)
(add-to-list 'package-archives
	     '("melpa" . "http://melpa.org/packages/") t)
(package-initialize)
(unless package-archive-contents
  (package-refresh-contents))
(when (not (package-installed-p 'use-package))
  (package-refresh-contents)
  (package-install 'use-package))

;; default to :ensure t
(require 'use-package-ensure)
(setq use-package-always-ensure t)


;; Theme
(use-package material-theme
  :defer t
  :init (load-theme 'material t)
  )

;; Packages
(use-package rainbow-delimiters
  :config
  (add-hook 'prog-mode-hook #'rainbow-delimiters-mode)
  )

(use-package rainbow-mode
  :init
  (define-globalized-minor-mode global-rainbow-mode rainbow-mode (lambda () (rainbow-mode 1)))
  (global-rainbow-mode 1)
  )

(use-package multiple-cursors
  :bind (
	 ("C-<mouse-1>" . mc/add-cursor-on-click)
	 ("C-S-c C-S-c" . mc/edit-lines)
	 ("C->" . mc/mark-next-like-this)
	 ("C-<" . mc/mark-previous-like-this)
	 ("C-c C-<" . mc/mark-all-like-this)
	 ("C-c C->" . mc/mark-all-like-this)
	 )
  :config
  (global-unset-key (kbd "C-<down-mouse-1>"))
  )

(use-package org-bullets
  :hook  (org-mode . org-bullets-mode)
  )

(use-package spaceline
  :init
  (require 'spaceline-config)
  :config
  (spaceline-spacemacs-theme)
  )

(use-package all-the-icons
  :if window-system
  :init
  (when (not (member "all-the-icons" (font-family-list)))
    (all-the-icons-install-fonts t))
  )

(use-package neotree
  :after all-the-icons
  :config
  (setq neo-theme (if (display-graphic-p) 'icons 'arrow))
  )


;; Python
(use-package elpy
  :ensure t
  :init
  (elpy-enable)
  :bind (
	 ("C-." . python-black-buffer)
         )
  )

;; (use-package python-black
;;   :defer t
;;   :hook (python-mode . python-black-on-save-mode)
;;   )


;; (use-package flycheck
;;   :defer t
;;   :init
;;   (global-flycheck-mode)
;;   )

(use-package exec-path-from-shell
  :defer t
  :init
  (exec-path-from-shell-copy-env "PATH")
  )


(use-package htmlize)
(use-package ox-reveal)

;; (use-package spaceline-all-the-icons 
;;   :after spaceline
;;   :config (spaceline-all-the-icons-theme)
;; )
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   '(csv org-re-reveal csv-mode markdown-preview-mode markdown-mode+ json-mode vue-mode dockerfile-mode go-mode toml-mode rust-mode php-mode markdown-mode htmlize org-reveal ox-reveal yaml-mode python-black exec-path-from-shell elpy neotree all-the-icons spaceline org-bullets multiple-cursors rainbow-mode rainbow-delimiters material-theme use-package)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
