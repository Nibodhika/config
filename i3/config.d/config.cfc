# the vars file needs to be first so that other config files can use the vars
# defined within it.
import vars.conf;

import base.conf; # Base i3 configurations
import shortcuts.conf; # Shortcut configuration
import workspaces.conf; # Workspaces
import colors.conf; # Colors
import bar.conf; # Navigation bar

try hostname $hostname: import ${hostname}.conf

import programs.conf; # Programs that will start together with i3
