script
   scripts_folder="/usr/lib/i3blocks"
   echo "# scripts folder is $scripts_folder"

   sensor_name=`sensors | head -1`
   echo "# sensor name is $sensor_name"
   

!script

import base.conf scripts_folder=$scripts_folder
chassis laptop: import battery.conf scripts_folder=$scripts_folder


text

#[temp]
#command=sensors | awk '/CPU/ {print substr(\$2,2,6)}'
#interval=10

[layout]
label=
command=~/config/i3/scripts/layout
interval=10

[time]
command=date '+%F %T'
interval=5

!text
